'use strict';

const listForms = [];

class Form {
    constructor(id, title, comment, fields){
        this.id         = id;
        this.title      = title;
        this.comment    = comment;
        this.fields     = fields;
    };
};

class Field {
    constructor (typeField, title, required, choices) {
        this.typeField  = typeField;
        this.title      = title;
        this.required   = required;
        this.choices    = choices;
    };
};

demo();
activeMenuForm(true);

function demo() {

    const listFields = [];
    const singleLine = new Field(
        'singleLine',
        'SSN',
        true,
        []
    );
    listFields.push(singleLine);

    const radioBtn = new Field(
        'radioBtn',
        'SSN',
        false,
        ['Yes','No']
    );
    listFields.push(radioBtn);

    const form1 = new Form (
                            1,
                            'San Francisco Driver Form',
                            'Welcom aboard!',
                            listFields);
    listForms.push(form1);
}

function activeMenuForm(isFields) {
    let elemAdd, elemRemove;
    if (isFields){
        elemAdd         = document.querySelector("div.menu-fields");
        elemRemove      = document.querySelector("div.menu-description");
    } else {
        elemAdd       = document.querySelector("div.menu-description");
        elemRemove    = document.querySelector("div.menu-fields");
    }
    if ( elemAdd === null || elemRemove === null ) return 0;

    if (elemAdd.classList.length === 1) {
        elemAdd.classList.add('active');
    };
    
    if (elemRemove.classList.length === 2) {
        elemRemove.classList.remove('active');
    };

    let panelBtn, panelDesc;
    if (isFields){
        panelBtn = document.querySelector("div.fields");
        panelDesc = document.querySelector("div.description");
        panelBtn.hidden = false;
        panelDesc.hidden = true;
    } else{
        panelBtn = document.querySelector("div.fields");
        panelDesc = document.querySelector("div.description");
        panelBtn.hidden = true;
        panelDesc.hidden = false;
    }

};

function clickOnButtomField(event) {

    let btn = event.target;
    
    if ( btn === null || btn === null ) return 0;

    let elemsRemove = document.querySelectorAll("button.my-button.active-b");

    for (let elem of elemsRemove){
        elem.classList.remove('active-b');
    };

    if (btn.classList.length === 1) {
        btn.classList.add('active-b');
    };

}